import Vue from 'vue';
import Profile from '../components/Profile.vue';
Vue.component('profile', Profile);
import Particles from 'particlesjs';

import Content from '../components/Content.vue';
Vue.component('ca-content', Content);

import Sidebar from '../components/Sidebar.vue';
Vue.component('ca-sidebar', Sidebar);
import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'babel-polyfill';
import Vuetify from 'vuetify'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faGithub } from '@fortawesome/free-brands-svg-icons'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faCoffee, faSpinner, faGithub);

Vue.component('font-awesome-icon', FontAwesomeIcon)

 
Vue.use(Vuetify)
/**
 *  ParticlesJS
 */
window.onload = function() {
    Particles.init(
    {
        selector: '.background',
        color: '#fff',

        retina_detect: true
      }
);
  };